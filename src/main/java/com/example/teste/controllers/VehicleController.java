package com.example.teste.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.teste.dtos.VehicleDto;
import com.example.teste.services.VehicleService;

@RestController
public class VehicleController {
	
	@Autowired
	VehicleService vS;
	
	@GetMapping("/vehicles")
	public ResponseEntity<List<VehicleDto>> getVehicles(){
		return new ResponseEntity<List<VehicleDto>>(vS.getVehicles(),HttpStatus.OK);
	}
	
	@GetMapping("/vehicles/{id}")
	public ResponseEntity<VehicleDto> getVehicle(@PathVariable Long id){
		return new ResponseEntity<VehicleDto>(vS.getVehicle(id),HttpStatus.OK);
	}
	
	@PostMapping("/vehicle")
	public void createVehicle(@RequestBody VehicleDto vehicle){
		vS.createVehicle(vehicle);	
	}
	
	@PutMapping("/vehicle/{id}")
	public void modifyVehicle(@RequestBody VehicleDto vehicle, @PathVariable Long id){
		vS.modifyVehicle(vehicle, id);
	}
	
	@DeleteMapping("/vehicle/{id}")
	public void deleteVehicle(@PathVariable Long id){
		vS.deleteVehicle(id);
	}


}
