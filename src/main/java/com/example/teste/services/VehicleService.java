package com.example.teste.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.teste.dtos.VehicleDto;
import com.example.teste.entities.VehicleEntity;
import com.example.teste.interfaces.VehicleInterface;
import com.example.teste.repositories.VehicleRepository;

@Service
public class VehicleService implements VehicleInterface {
	
	@Autowired
	VehicleRepository vR;
	
	//@Autowired
	//VehicleConverter converter;

	@Override
	public VehicleDto getVehicle(Long id) {
		VehicleEntity vehicle = vR.findById(id).get();
		return convert2Dto(vehicle);
	}

	@Override
	public List<VehicleDto> getVehicles(){	
		return convertList(vR.findAll());
	}

	@Override
	public void createVehicle(VehicleDto vehicle) {
		VehicleEntity vehi = convert2Entity(vehicle);
		vR.save(vehi);
		
	}

	@Override
	public void deleteVehicle(Long id) {
		vR.delete(vR.findById(id).get());
				
	}

	@Override
	public void modifyVehicle(VehicleDto vehicle, Long id) {
		VehicleEntity vehi = vR.findById(id).get();
		vehi.setBrand(vehicle.getBrand());
		vehi.setCreat_at(vehicle.getCreat_at());
		vehi.setDescription(vehicle.getDescription());
		vehi.setID(vehicle.getID());
		vehi.setSold(vehicle.getSold());
		vehi.setUpd_at(vehicle.getUpd_at());
		vehi.setVehicle(vehicle.getVehicle());
		vehi.setYear(vehicle.getYear());
		vR.save(vehi);
		
	}
	
	
	private VehicleDto convert2Dto(VehicleEntity source) {
		VehicleDto vehicle = new VehicleDto();
		vehicle.setID(source.ID);
		vehicle.setBrand(source.brand);
		vehicle.setCreat_at(source.creat_at);
		vehicle.setDescription(source.description);
		vehicle.setSold(source.sold);
		vehicle.setUpd_at(source.upd_at);
		vehicle.setVehicle(source.vehicle);
		vehicle.setYear(source.year);
		return vehicle;
	}
	
	
	private VehicleEntity convert2Entity(VehicleDto source) {
		VehicleEntity vehicle = new VehicleEntity();
		vehicle.setID(source.ID);
		vehicle.setBrand(source.brand);
		vehicle.setCreat_at(source.creat_at);
		vehicle.setDescription(source.description);
		vehicle.setSold(source.sold);
		vehicle.setUpd_at(source.upd_at);
		vehicle.setVehicle(source.vehicle);
		vehicle.setYear(source.year);
		return vehicle;
	}
	
	private List<VehicleDto> convertList(List<VehicleEntity> source){
		List<VehicleDto> output = new ArrayList<>();
		for(VehicleEntity elem : source) {
			output.add(convert2Dto(elem));
		}
		return output;
	}

}
