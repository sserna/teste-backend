package com.example.teste.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "VEHICLES")
@Entity
public class VehicleEntity {
	
	@Id
	@SequenceGenerator(name = "idVehicle", sequenceName = "idVehicle", initialValue = 2, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idVehicle")
	@Column(name = "ID")
	public Long ID;
	
	@Column(name = "BRAND")
	public String brand;
	
	@Column(name = "VEHICLE")
	public String vehicle;
	
	@Column(name = "YEAR")
	public Integer year;
	
	@Column(name = "DESCRIPTION")
	public String description;
	
	@Column(name = "SOLD")
	public Boolean sold;
	
	@Column(name = "CREATION_DATE")
	public Date creat_at;
	
	@Column(name = "UPDATE_DATE")
	public Date upd_at;	

}
