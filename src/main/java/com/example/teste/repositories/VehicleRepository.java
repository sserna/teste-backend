package com.example.teste.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teste.entities.VehicleEntity;


public interface VehicleRepository extends JpaRepository<VehicleEntity, Long>{

}
