package com.example.teste.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.core.convert.converter.Converter;

import com.example.teste.dtos.VehicleDto;
import com.example.teste.entities.VehicleEntity;
import com.example.teste.repositories.VehicleRepository;





public class VehicleConverter implements Converter<VehicleEntity, VehicleDto>{
	
	@Autowired
	VehicleRepository vR;
	

	@Override
	public VehicleDto convert(VehicleEntity source) {
		VehicleDto vehicle = new VehicleDto();
		vehicle.setID(source.ID);
		vehicle.setBrand(source.brand);
		vehicle.setCreat_at(source.creat_at);
		vehicle.setDescription(source.description);
		vehicle.setSold(source.sold);
		vehicle.setUpd_at(source.upd_at);
		vehicle.setVehicle(source.vehicle);
		vehicle.setYear(source.year);
		return vehicle;
	}
	
	
	public VehicleEntity convert2Entity(VehicleDto source) {
		VehicleEntity vehicle = new VehicleEntity();
		vehicle.setID(source.ID);
		vehicle.setBrand(source.brand);
		vehicle.setCreat_at(source.creat_at);
		vehicle.setDescription(source.description);
		vehicle.setSold(source.sold);
		vehicle.setUpd_at(source.upd_at);
		vehicle.setVehicle(source.vehicle);
		vehicle.setYear(source.year);
		return vehicle;
	}
	
	public List<VehicleDto> convert(List<VehicleEntity> source){
		List<VehicleDto> output = new ArrayList<>();
		for(VehicleEntity elem : source) {
			output.add(convert(elem));
		}
		return output;
	}

}
