package com.example.teste.dtos;

import java.util.Date;

import lombok.Data;

@Data
public class VehicleDto {
	
	public Long ID;
	
	public String brand;
	
	public String vehicle;
	
	public Integer year;
	
	public String description;
	
	public Boolean sold;
	
	public Date creat_at;
	
	public Date upd_at;	

}
