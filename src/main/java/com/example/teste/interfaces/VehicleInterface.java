package com.example.teste.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.teste.dtos.VehicleDto;

@Service
public interface VehicleInterface {
	
	public VehicleDto getVehicle(Long id);
	
	public List<VehicleDto> getVehicles();
	
	public void createVehicle(VehicleDto vehicle);
	
	public void deleteVehicle(Long id);
	
	public void modifyVehicle(VehicleDto vehicle, Long id);
	

}
